

ranks = {}

ranks.player = {}
ranks.boxes = {}
ranks.builder = {}
ranks.categories = {}

local wp = minetest.get_worldpath()

local fetch_file = function(filename, old, verify_ranks)
	local f = io.open(filename, "r")
	if not f then
		minetest.log("error", "failed to open " .. filename)
		return old
	end

	local fr = f:read("*all")
	f:close()

	local pl = minetest.parse_json(fr)
	if not pl then
		minetest.log("error", "failed to parse " .. filename)
		return old
	end

	if not verify_ranks then
		return pl
	end

	local tbl = {}

	for i = 1, 10 do
		if pl[tostring(i)] then
			tbl[i] = pl[tostring(i)]
		else
			minetest.log("error", filename .. " missing rank #" .. i)
			return old
		end
	end

	return tbl
end


ranks.fetch = function()
	ranks.player = fetch_file(wp .. "/top_players.json", ranks.player, true)
	ranks.box = fetch_file(wp .. "/top_boxes.json", ranks.box, true)
	ranks.builder = fetch_file(wp .. "/top_builders.json", ranks.builder, true)

	ranks.categories = fetch_file(wp .. "/category_series.json", ranks.categories, false)

	minetest.after(300, ranks.fetch)
end

minetest.after(0, ranks.fetch)
