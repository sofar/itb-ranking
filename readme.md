
## itb-ranking

This project runs parallel to the Minetest ITB server and processes the `points`
table in `itb.sqlite`. It creates weighted ranks for players, builders and boxes
and outputs JSON data. The current ITB website ingests this data and displays
it.

The points data is very, very large, and will continue to grow over time. For this
reason there is an out-of-MT process doing this. Doing this in lua would require
too much memory and CPU time. Currently, it takes a few minutes to run this task.

## License

The code is licensed MIT. It is technically trivial code, the mathmatics are very
simple and there isn't much to it.

